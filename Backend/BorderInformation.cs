﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Microsoft.Phone;
using System.Windows.Resources;
using HtmlAgilityPack;
using System.Linq;
using System.Collections.Generic;
using QDFeedParser;

namespace BorderInformation
{
    public class Garita
    {
        /* These constansts probably belong somewehere else, but just leaving them here for now. */
        public const string SAN_LUIS = "SanLuis";
        public const string ALGODONES = "Algodones";
        public const string MEXICALI = "Mexicali";
        private const string SOURCE_URL = "http://garitas.mx/";
        private const string BASE_URL = "http://falling-rain-4023.herokuapp.com/";

        /* Class attributes */
        private string borderName;
        private string url;
        private int viewPosition;
        private WriteableBitmap garitaView;
        private string waitTimes;

        public Garita()
        {
            borderName = SAN_LUIS;
            viewPosition = 1;
            url = "";
            garitaView = null;
        }

        /// <summary>
        /// Obtain the next image from the server in asynchronous manner to not tie up the main thread.
        /// </summary>
        /// <returns>garitaView</returns>
        public async Task<WriteableBitmap> getGaritaView()
        {
            try
            {
                Random random = new Random();
                url = BASE_URL + borderName.ToLower() + "1_" + viewPosition;
                Stream viewStream = await new WebClient().OpenReadTaskAsync( new Uri(url + "?q=" + random.Next().ToString() ));
                garitaView = PictureDecoder.DecodeJpeg(viewStream);
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }
            return garitaView;
        }
        
        public async Task<string> getWaitingTimes()
        {
            url = SOURCE_URL + borderName + ".html";
            Stre
            //string htmlSource = await new WebClient().DownloadStringTaskAsync(new Uri(url));
            HtmlWeb.LoadAsync(@url, DownloadCompleted);
            //document.LoadHtml(htmlSource);
            //// Do stuff with it
            //Debug.WriteLine(htmlSource);

            return null;
        }

        private void DownloadCompleted( object sender, HtmlDocumentLoadCompleted e )
        {
            if ( e.Error == null )
            {
                HtmlDocument doc = e.Document;
                if ( doc != null )
                {
                    IEnumerable<HtmlNode> dataDiv = from datanode in doc.DocumentNode.Descendants()
                                                    where datanode.Name == "div"
                                                    && datanode.Attributes["class"].Value == "galleria-info-description"
                                                    select datanode.FirstChild;

                    if ( dataDiv != null )
                    {
                        string divurl = dataDiv.ElementAt(1).InnerText;
                        MessageBox.Show(divurl);
                    }
                }

            }
        }

        public string BorderName
        {
            get { return borderName; }
            set { borderName = value; }
        }

        public int ViewPosition
        {
            get { return viewPosition; }
            set { viewPosition = (value < 2 || value > 1) ? 1 : value; }
        }
    }
}
